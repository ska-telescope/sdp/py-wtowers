from setuptools import setup, find_packages, Extension
import os

CFLAGS_SETUP = " -std=c99 -Wall -g -O3 -ftree-vectorize -ffast-math -fopenmp -fPIC"# -DOMP_TOWERS"
LDFLAGS_SETUP = " -g -Wl,--no-as-needed -lgomp -lhdf5 -lfftw3 -lfftw3_threads -lm -lc -ldl"

try:
	os.environ['CFLAGS'] = os.environ['CFLAGS'] + CFLAGS_SETUP
except:
	os.environ['CFLAGS'] = CFLAGS_SETUP

try: 
	os.environ['LDFLAGS'] = os.environ['LDFLAGS'] + LDFLAGS_SETUP
except:
	os.environ['LDFLAGS'] = LDFLAGS_SETUP
	

extensions = Extension("grid.libgrid",
                       ["src/grid.c", "src/hdf5.c"],
                       depends=["src/grid.h"],
                       include_dirs=["src"]
              )


setup (name = 'wtowers',
       version = '0.12',
       description = 'This is a wtowers package',
       author = 'Vlad Stolyarov',
       author_email = 'vlad.stolyarov@gmail.com',
       url = 'https://gitlab.com/ska-telescope/py-wtowers',
       long_description = '''
Gridding/degridding package based on https://github.com/KentJames/Radio-Gridders/ by James Kent (Cambridge)
''',
       packages=['wtowers'],
       ext_modules = [extensions])
