CC = gcc
override CFLAGS += -I$(HDF5_INC) -std=c99 -Wall -g -O3 -ftree-vectorize -ffast-math -fopenmp -fPIC #-DOMP_TOWERS
override LDFLAGS += -L$(HDF5_LIB) -g -lgomp -lhdf5 -lfftw3 -lfftw3_threads -lm -lc -ldl

all: libgrid.so grid_CPU.out grid_CPU_shared.out


clean: 
		rm -f src/*.o
		rm -f *.out
		rm -f *.so

grid_CPU.out: src/main.o src/hdf5.o src/grid.o
		$(CC) src/main.o src/hdf5.o src/grid.o -o grid_CPU.out $(LDFLAGS)

grid_CPU_shared.out: src/main.o
		$(CC) src/main.o -o grid_CPU_shared.out -L ./ -lgrid $(LDFLAGS)

main.o: src/main.c
		$(CC) $(CFLAGS) -c src/main.c

hdf5.o: src/hdf5.c 
		$(CC) $(CFLAGS) -c src/hdf5.c

grid.o: src/grid.c
		$(CC) $(CFLAGS) -c src/grid.c

libgrid.so: src/hdf5.o src/grid.o
		$(CC) -shared src/hdf5.o src/grid.o -o libgrid.so $(LDFLAGS)


wtowers_test: 
	./grid_CPU.out --theta=0.08 --lambda=300000 --wkern=../Radio-Gridders/data/crocodile_data/kernels/SKA1_Low_wkern.h5 --subgrid=100 --margin=20 --winc=10 ../Radio-Gridders/data/crocodile_data/vis/SKA1_Low_vis.h5

wtowers_test_vla: 
	./grid_CPU.out --theta=0.1 --lambda=20000 --image=image_towers.out --wkern=../Radio-Gridders/data/crocodile_data/kernels/vla_w20_static_size15.h5 --subgrid=100 --margin=20 --winc=20 ../Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5

test:
	./grid_CPU.out --theta=0.1 --lambda=20000 --image=image_proj.out ../Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5

.PHONY: all clean test
