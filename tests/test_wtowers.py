import matplotlib.pyplot as plt
import numpy as np
import os
from time import time
import ctypes

import pprint
import h5py
import wtowers.wtowers as wtowers

plt.rcParams['figure.figsize'] = [12, 8]

h5name = "/home/vlad/software/SKA/Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5"
h5name_wkern = "/home/vlad/software/SKA/Radio-Gridders/data/crocodile_data/kernels/vla_w20_static_size15.h5"

min_len = -1.79768e+308
max_len = 1.79769e+308
vis = wtowers.VIS_DATA()

wtowers.init_dtype_cpx_func()
status = wtowers.load_vis_func(h5name,vis, min_len, max_len)
print("vis.antenna_count: ", vis.antenna_count)


theta = 0.1
uvlambda = 20000.
uvmax = wtowers.check_grid_size_func(vis,uvlambda)
if uvmax !=0:
	exit()

subgrid_size = 100
margin=20
winc=20
grid_size = int(theta*uvlambda)
print("Grid size is ", grid_size, "x", grid_size)

wkern = wtowers.W_KERNEL_DATA()
status = wtowers.load_wkern_func(h5name_wkern, theta, wkern)

uvgrid = np.zeros(grid_size*grid_size, dtype=np.complex128)
wtowers.weight_func(grid_size, theta, vis)
flops = wtowers.grid_wtowers_func(uvgrid, grid_size, theta, vis, wkern, subgrid_size, margin, winc)
wtowers.make_hermitian_func(uvgrid, grid_size)

uvgrid = uvgrid.reshape((grid_size,grid_size))
img = np.fft.fft2(np.fft.fftshift(uvgrid))
img = np.fft.fftshift(img)
dirty = np.real(img)
dirtymax = np.amax(dirty)
dirtymin = np.amin(dirty)
print(dirtymin,dirtymax)
plt.imshow(dirty, vmin=0, vmax = dirtymax/50.)
#plt.rcParams['figure.figsize'] = [10, 5]
plt.show()
