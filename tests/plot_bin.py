import matplotlib.pyplot as plt
import numpy as np
import sys

plt.rcParams['figure.figsize'] = [12, 8]

if len(sys.argv) != 2 and len(sys.argv) != 4:
        print("Usage: plot_bin.py filename <vmin_ratio vmax_ratio>")
        sys.exit()
print("The filename is %s" % (sys.argv[1]))
fname = sys.argv[1]

binimage = np.fromfile(fname, dtype='double')
grid_size = int(np.sqrt(len(binimage)))
print("Grid size is", grid_size, "by", grid_size)
binimage = binimage.reshape((grid_size,grid_size))
pmax = np.amax(binimage)
pmin = np.amin(binimage)
print("MIN, MAX:", pmin, pmax)
if(len(sys.argv) !=4):
	vmin_ratio = 0.05
	vmax_ratio = 0.05
else:
	vmin_ratio = np.double(sys.argv[2])
	if vmin_ratio > 1.0 or vmin_ratio < 0.0:
		print("Wrong vmin_ratio, resetting to 1.0")
		vmin_ratio = 1.0
	vmax_ratio = np.double(sys.argv[3])
	if vmax_ratio > 1.0 or vmax_ratio < 0.0:
		print("Wrong vmax_ratio, resetting to 1.0")
		vmax_ratio = 1.0

print(vmin_ratio, vmax_ratio)
plt.imshow(binimage,vmin = pmin*vmin_ratio, vmax = pmax*vmax_ratio)
plt.colorbar()
plt.show()

