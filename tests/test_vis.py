import numpy as np
import os
from time import time
import ctypes
import copy

import pprint
import h5py
import wtowers.wtowers as wtowers

#h5name = "/alaska/vlad/software1/Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5"
h5name = "/home/vlad/software/SKA/Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5"

min_len = -1.79768e+308
max_len = 1.79769e+308
vis = wtowers.VIS_DATA()

print("Reading",h5name)
wtowers.init_dtype_cpx_func()
status = wtowers.load_vis_func(h5name,vis, min_len, max_len)
print("vis.antenna_count: ", vis.antenna_count)
print("vis.bl_count: ", vis.bl_count)

print("Copying vis to vis1...")
vis1 = wtowers.VIS_DATA()
status = wtowers.copy_vis_data_func(vis1,vis)
print("vis1.antenna_count: ", vis1.antenna_count)
print("vis1.bl_count: ", vis1.bl_count)

blmax = vis.bl_count
print("Comparing some data in both vis_data sets")
assert(vis.bl[0].vis[0]==vis1.bl[0].vis[0])
assert(vis.bl[blmax-1].vis[vis.bl[blmax-1].time_count*vis.bl[blmax-1].freq_count-1]==vis1.bl[blmax-1].vis[vis1.bl[blmax-1].time_count*vis1.bl[blmax-1].freq_count-1])

assert(vis.bl[blmax-1].uvw[0:3] == vis1.bl[blmax-1].uvw[0:3])
print("vis.bl[",blmax-1,"].uvw[0:3]=",vis.bl[blmax-1].uvw[0:3])
print("vis1.bl[",blmax-1,"].uvw[0:3]=",vis1.bl[blmax-1].uvw[0:3])

print("Changing the second vis_data set...")
vis1.bl[blmax-1].uvw[0] = 1.
vis1.bl[blmax-1].uvw[1] = 2.
vis1.bl[blmax-1].uvw[2] = 3.
assert(vis.bl[blmax-1].uvw[0:3] != vis1.bl[blmax-1].uvw[0:3])
print("vis.bl[",blmax-1,"].uvw[0:3]=",vis.bl[blmax-1].uvw[0:3])
print("vis1.bl[",blmax-1,"].uvw[0:3]=",vis1.bl[blmax-1].uvw[0:3])
print("copy_vis_data_func test PASS")

print("Copying vis to vis1, erasing visibilities")
vis1 = wtowers.VIS_DATA()
status = wtowers.copy_vis_data_func(vis1,vis,1)
print("vis1.antenna_count: ", vis1.antenna_count)
print("vis1.bl_count: ", vis1.bl_count)
print("Checking some visibility data...")
print("vis.bl[0].vis[0]=",vis1.bl[0].vis[0],vis1.bl[0].vis[1])
print("vis1.bl[",blmax-1,"].vis[0]=",vis1.bl[blmax-1].vis[0],vis1.bl[blmax-1].vis[1])
assert((vis1.bl[blmax-1].vis[0] == 0.0) and (vis1.bl[blmax-1].vis[1] == 0.0) )
print("copy_vis_data_func with erasing visibilities test PASS")


print("Creating a new vis_data set with the same bl_count and bl[].time_count,")
print("and bl[].freq_count = 1 for all blocks")

vis2 = wtowers.VIS_DATA()
wtowers.fill_vis_data_func(vis2,vis.antenna_count,vis.bl_count, vis.bl[0].time_count, 1)
assert(vis.bl[0].uvw[0:3] != vis2.bl[0].uvw[0:3])

for i in range(vis2.bl_count):
	vis2.bl[i].antenna1 = vis.bl[i].antenna1
	vis2.bl[i].antenna2 = vis.bl[i].antenna2
	vis2.bl[i].freq_count = 1
	vis2.bl[i].freq[0] = 100.

	for j in range(vis.bl[i].time_count):
		vis2.bl[i].uvw[3*j] = vis.bl[i].uvw[3*j]
		vis2.bl[i].uvw[3*j+1] = vis.bl[i].uvw[3*j+1]
		vis2.bl[i].uvw[3*j+2] = vis.bl[i].uvw[3*j+2]
		vis2.bl[i].time[j] = vis.bl[i].time[j]

assert(vis.bl[blmax-1].uvw[0:3] == vis2.bl[blmax-1].uvw[0:3])
print("Changing the new vis_data set...")
vis2.bl[blmax-1].uvw[0] = 1.
vis2.bl[blmax-1].uvw[1] = 2.
vis2.bl[blmax-1].uvw[2] = 3.
assert(vis.bl[blmax-1].uvw[0:3] != vis2.bl[blmax-1].uvw[0:3])
print("fill_vis_data_func test PASS")






