import matplotlib.pyplot as plt
import numpy as np
import os
from time import time
import ctypes
import copy

import pprint
import h5py
import wtowers.wtowers as wtowers

plt.rcParams['figure.figsize'] = [12, 8]

#h5name = "/alaska/vlad/software1/Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5"
#h5name_wkern = "/alaska/vlad/software1/Radio-Gridders/data/crocodile_data/kernels/vla_w20_static_size15.h5"
h5name = "/home/vlad/software/SKA/Radio-Gridders/data/crocodile_data/vis/vlaa_theta0.1.h5"
h5name_wkern = "/home/vlad/software/SKA/Radio-Gridders/data/crocodile_data/kernels/vla_w20_static_size15.h5"


min_len = -1.79768e+308
max_len = 1.79769e+308
vis = wtowers.VIS_DATA()

wtowers.init_dtype_cpx_func()
status = wtowers.load_vis_func(h5name,vis, min_len, max_len)
print("vis.antenna_count: ", vis.antenna_count)

theta = 0.1
uvlambda = 20000.
uvmax = wtowers.check_grid_size_func(vis,uvlambda)
if uvmax !=0:
	exit()

subgrid_size = 100
margin=20
winc=20
grid_size = int(theta*uvlambda)
print("Grid size is ", grid_size, "x", grid_size)

wkern = wtowers.W_KERNEL_DATA()
status = wtowers.load_wkern_func(h5name_wkern, theta, wkern)

uvgrid = np.zeros(grid_size*grid_size, dtype=np.complex128)
# Apply a uniform weighting
wtowers.weight_func(grid_size, theta, vis)
# Make simple gridding
flops = wtowers.grid_simple_func(uvgrid, grid_size, theta, vis)
# Fill a hermitian conjugated part of the uv_grid plane
wtowers.make_hermitian_func(uvgrid, grid_size)

# Create a dirty image and show 
uvgrid = uvgrid.reshape((grid_size,grid_size))
img = np.fft.fft2(np.fft.fftshift(uvgrid))
img = np.fft.fftshift(img)
dirty = np.real(img)
dirtymax = np.amax(dirty)
dirtymin = np.amin(dirty)
print("Dirty min and max: ", dirtymin,dirtymax)
plt.imshow(dirty, vmin=0, vmax = dirtymax/10.)
#plt.imshow(dirty)
plt.colorbar()
plt.savefig('Simple_grid_dirty.png')
plt.show()

# Create de-gridded visibility structure with the same times and uvw
vis_degrid = wtowers.VIS_DATA()
wtowers.copy_vis_data_func(vis_degrid, vis,1)
#wtowers.fill_vis_data_func(vis_degrid,vis.antenna_count,vis.bl_count, vis.bl[0].time_count, 1)
#vis_degrid.antenna_count = vis.antenna_count
#vis_degrid.bl_count = vis.bl_count
#vis_degrid.bl = vis.bl
# Find the mean frequency

print(vis.antenna_count, vis.bl_count,vis.bl[0].time_count)
print(vis_degrid.antenna_count, vis_degrid.bl_count,vis_degrid.bl[0].time_count )
assert(vis_degrid.bl_count == vis.bl_count)

#freq_degrid = np.mean(np.ctypeslib.as_array(vis.bl[0].freq, shape=(vis.bl[0].freq_count,)))
#print(vis.bl[vis.bl_count-1].time[0], vis.bl[vis.bl_count-1].freq_count, vis.bl[vis.bl_count-1].freq[0])

# Replace all the freqquency counts with 1, and freq[0] with freq_degrid
#for i in range(vis_degrid.bl_count):
#	vis_degrid.bl[i].antenna1 = vis.bl[i].antenna1
#	vis_degrid.bl[i].antenna2 = vis.bl[i].antenna2
	
#	vis_degrid.bl[i].freq_count = 1
#	vis_degrid.bl[i].freq[0] = freq_degrid
#	vis_degrid.bl[i].uvw = vis.bl[i].uvw
#	vis_degrid.bl[i].time = vis.bl[i].time

# ToDo deep copy of the visibilities, otherwise vis data is lost.
#print(vis_degrid.bl[vis_degrid.bl_count-1].time[0], vis_degrid.bl[vis_degrid.bl_count-1].freq_count, vis_degrid.bl[vis_degrid.bl_count-1].freq[0])
#print(vis.bl[vis.bl_count-1].time[0], vis.bl[vis.bl_count-1].freq_count, vis.bl[vis.bl_count-1].freq[0])

# Degrid back and weight again
print("De-gridding back and apply uniform weighting again...")
wtowers.degrid_simple_func(vis_degrid,uvgrid.reshape((grid_size*grid_size)),grid_size, theta)
wtowers.weight_func(grid_size, theta, vis_degrid)
# Make a simple gidding again
print("Gridding again...")
uvgrid1 = np.zeros(grid_size*grid_size, dtype=np.complex128)
flops = wtowers.grid_simple_func(uvgrid1, grid_size, theta, vis_degrid)

# Fill a hermitian conjugated part of the uv_grid plane
wtowers.make_hermitian_func(uvgrid1, grid_size)

# Create a dirty image and show 
uvgrid1 = uvgrid1.reshape((grid_size,grid_size))
img = np.fft.fft2(np.fft.fftshift(uvgrid1))
img = np.fft.fftshift(img)
dirty = np.real(img)
dirtymax = np.amax(dirty)
dirtymin = np.amin(dirty)
print("Dirty min and max: ", dirtymin,dirtymax)
plt.clf()
plt.imshow(dirty, vmin=0, vmax = dirtymax/10.)
plt.colorbar()
plt.savefig('Simple_grid-degrid-grid_dirty.png')
plt.show()
	





