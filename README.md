# py-wtowers

py-wtowers repository 

Gridding algorithms for the radio interferometry. The C code is inherited from https://github.com/KentJames/Radio-Gridders/tree/master/src/reference, the functions are wrapped using ctypes.

## Usage

1. Build and install the `wtowers` module with `python3 setup.py build; python3 setup.py install` ;
2. Go to `tests` and change the path to HDF5 files in `test_*.py` . The files can be taken from https://github.com/KentJames/Radio-Gridders/tree/master/data/crocodile_data ;
3. Check if the required python modules are installed (`matplotlib`, `numpy`, `h5py`) ;
4. Run the tests, e.g. `python test_wtowers.py`. 
