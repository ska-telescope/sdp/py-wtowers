""" First version of wrapping a C gridding library functions using the numpy.ctypeslib. """

import numpy as np
import numpy.ctypeslib as npct
import ctypes
from ctypes.util import find_library
import os
import imp

# input type for the most functions
# must be a double or complex array, with single dimension that is contiguous
array_1d_double = npct.ndpointer(dtype=np.double, ndim=1, flags='CONTIGUOUS')
array_1d_complex = npct.ndpointer(dtype=np.complex128, ndim=1, flags='CONTIGUOUS')
array_1d_uint = npct.ndpointer(dtype=np.uint, ndim=1, flags='CONTIGUOUS')


# Locate the libgrid library
wtowers_path = imp.find_module('wtowers')[1]
libgrid_path = wtowers_path + "/../grid/"
libgrid_name = []
for file in os.listdir(libgrid_path):
    if file.endswith(".so"):
       libgrid_name.append(file)
print(libgrid_path, libgrid_name[0])

# load the library (1st *.so file), using numpy mechanisms
libcd = npct.load_library(libgrid_name[0],libgrid_path )
#libcd = npct.load_library("libgrid.cpython-36m-x86_64-linux-gnu.so", ".")

#Block Visibility data structure 
'''
struct bl_data
{
    int antenna1, antenna2;
    int time_count;
    int freq_count;
    double *time;
    double *freq;
    double *uvw;
    double complex *vis;
    double complex *awkern;

    double u_min, u_max; // in m
    double v_min, v_max; // in m
    double w_min, w_max; // in m
    double t_min, t_max; // in h
    double f_min, f_max; // in Hz

    uint64_t flops;
};

struct vis_data
{
    int antenna_count;
    int bl_count;
    struct bl_data *bl;
};
'''
class BL_DATA(ctypes.Structure):
    _fields_ = [
        ('antenna1', ctypes.c_int),
        ('antenna2', ctypes.c_int),
        ('time_count', ctypes.c_int),
        ('freq_count', ctypes.c_int),
	('time',ctypes.POINTER(ctypes.c_double)),
	('freq',ctypes.POINTER(ctypes.c_double)),
	('uvw',ctypes.POINTER(ctypes.c_double)),
	('vis',ctypes.POINTER(ctypes.c_double)),    # Complex
	('awkern',ctypes.POINTER(ctypes.c_double)), # Complex
	('u_min',ctypes.c_double),
	('u_max',ctypes.c_double),
	('v_min',ctypes.c_double),
	('v_max',ctypes.c_double),
	('w_min',ctypes.c_double),
	('w_max',ctypes.c_double),
	('t_min',ctypes.c_double),
	('t_max',ctypes.c_double),
	('f_min',ctypes.c_double),
	('f_max',ctypes.c_double),
	('flops',ctypes.c_uint64)
        ]

class VIS_DATA(ctypes.Structure):
    _fields_ = [
        ('antenna_count', ctypes.c_int),
	('bl_count', ctypes.c_int),
        ('bl', ctypes.POINTER(BL_DATA))
        ]


#// Static W-kernel data
'''
struct w_kernel
{
    double complex *data;
    double w;
};

struct w_kernel_data
{
    int plane_count;
    struct w_kernel *kern;
    struct w_kernel *kern_by_w;
    double w_min, w_max, w_step;
    int size_x, size_y;
    int oversampling;
};
'''
class W_KERNEL(ctypes.Structure):
    _fields_ = [
	('data',ctypes.POINTER(ctypes.c_double)),    # Complex
	('w', ctypes.c_double)
        ]

class W_KERNEL_DATA(ctypes.Structure):
    _fields_ = [
	('plane_count', ctypes.c_int),
	('kern', ctypes.POINTER(W_KERNEL)),
	('kern_by_w', ctypes.POINTER(W_KERNEL)),
	('w_min',ctypes.c_double),
	('w_max',ctypes.c_double),
	('w_step',ctypes.c_double),
        ('size_x', ctypes.c_int),
        ('size_y', ctypes.c_int),
        ('oversampling', ctypes.c_int)
        ]

# A-kernel data
'''
struct a_kernel
{
    double complex *data;
    int antenna;
    double time;
    double freq;
};
struct a_kernel_data
{
    int antenna_count, time_count, freq_count;
    struct a_kernel *kern;
    struct a_kernel *kern_by_atf;
    double t_min, t_max, t_step;
    double f_min, f_max, f_step;
    int size_x, size_y;
};
'''
class A_KERNEL(ctypes.Structure):
    _fields_ = [
	('data',ctypes.POINTER(ctypes.c_double)),    # Complex
	('antenna', ctypes.c_int),
	('time', ctypes.c_double),
	('freq', ctypes.c_double)
        ]

class A_KERNEL_DATA(ctypes.Structure):
    _fields_ = [
	('antenna_count', ctypes.c_int),
	('time_count', ctypes.c_int),
	('freq_count', ctypes.c_int),
	('kern', ctypes.POINTER(A_KERNEL)),
	('kern_by_atf', ctypes.POINTER(A_KERNEL)),
	('t_min',ctypes.c_double),
	('t_max',ctypes.c_double),
	('t_step',ctypes.c_double),
	('f_min',ctypes.c_double),
	('f_max',ctypes.c_double),
	('f_step',ctypes.c_double),
        ('size_x', ctypes.c_int),
        ('size_y', ctypes.c_int)
	]

# Initialization of HDF5
#void init_dtype_cpx()
libcd.init_dtype_cpx.restype  =  None
libcd.init_dtype_cpx.argtypes = None

def init_dtype_cpx_func():
	return libcd.init_dtype_cpx()

# A function to read visibilities from the HDF5 file
# int load_vis(const char *filename, struct vis_data *vis, double min_len, double max_len)
libcd.load_vis.restype  =  ctypes.c_int
libcd.load_vis.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.POINTER(VIS_DATA), ctypes.c_double, ctypes.c_double]
def load_vis_func(filename, out_struct, min_len, max_len):
    return libcd.load_vis(filename.encode(), out_struct, min_len, max_len)

# A function to read w-kernel from HDF5 file
# int load_wkern(const char *filename, double theta, struct w_kernel_data *wkern);
libcd.load_wkern.restype  =  ctypes.c_int
libcd.load_wkern.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_double, ctypes.POINTER(W_KERNEL_DATA)]
def load_wkern_func(filename, theta, out_struct):
    return libcd.load_wkern(filename.encode(), theta, out_struct)

# A function to read a-kernel from HDF5 file
# int load_wkern(const char *filename, double theta, struct a_kernel_data *akern);
libcd.load_akern.restype  =  ctypes.c_int
libcd.load_akern.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.c_double, ctypes.POINTER(A_KERNEL_DATA)]
def load_akern_func(filename, theta, out_struct):
    return libcd.load_akern(filename.encode(), theta, out_struct)

# A simple gridding function
# uint64_t grid_simple(double complex *uvgrid, int grid_size, double theta, struct vis_data *vis);
libcd.grid_simple.restype  =  ctypes.c_uint64
libcd.grid_simple.argtypes = [array_1d_complex, ctypes.c_int, ctypes.c_double, ctypes.POINTER(VIS_DATA)]
def grid_simple_func(uvgrid, grid_size, theta, out_struct):
    return libcd.grid_simple(uvgrid, grid_size, theta, out_struct)

# A simple de-gridding function
# uint64_t degrid_simple(struct vis_data *vis, double complex *uvgrid, int grid_size, double theta);
libcd.degrid_simple.restype  =  ctypes.c_uint64
libcd.degrid_simple.argtypes = [ctypes.POINTER(VIS_DATA), array_1d_complex, ctypes.c_int, ctypes.c_double]
def degrid_simple_func(out_struct, uvgrid, grid_size, theta):
    return libcd.degrid_simple(out_struct, uvgrid, grid_size, theta)

# W-towers gridding
# uint64_t grid_wtowers(double complex *uvgrid, int grid_size, double theta,
#                struct vis_data *vis, struct w_kernel_data *wkern,
#		int subgrid_size, int subgrid_margin, double wincrement);
libcd.grid_wtowers.restype  =  ctypes.c_uint64
libcd.grid_wtowers.argtypes = [array_1d_complex, ctypes.c_int, ctypes.c_double, ctypes.POINTER(VIS_DATA),\
			       ctypes.POINTER(W_KERNEL_DATA), ctypes.c_int, ctypes.c_int, ctypes.c_double]
def grid_wtowers_func(uvgrid, grid_size, theta, vis_struct, wkern_struct, subgrid_size, subgrid_margin, wincrement):
    return libcd.grid_wtowers(uvgrid, grid_size, theta, vis_struct, wkern_struct, subgrid_size, subgrid_margin, wincrement)

# W-towers de-gridding
#uint64_t degrid_wtowers(struct vis_data *vis, double complex *uvgrid, int grid_size,
#                      double theta, struct w_kernel_data *wkern,
#                      int subgrid_size, int subgrid_margin, double wincrement);
libcd.degrid_wtowers.restype  =  ctypes.c_uint64
libcd.degrid_wtowers.argtypes = [ctypes.POINTER(VIS_DATA), array_1d_complex, ctypes.c_int, ctypes.c_double, \
			       ctypes.POINTER(W_KERNEL_DATA), ctypes.c_int, ctypes.c_int, ctypes.c_double]
def degrid_wtowers_func(vis_struct, uvgrid, grid_size, theta, wkern_struct, subgrid_size, subgrid_margin, wincrement):
    return libcd.degrid_wtowers(vis_struct, uvgrid, grid_size, theta, wkern_struct, subgrid_size, subgrid_margin, wincrement)

# W-projection gridding
# uint64_t grid_wprojection(double complex *uvgrid, int grid_size, double theta, struct vis_data *vis, struct w_kernel_data *wkern);
libcd.grid_wprojection.restype  =  ctypes.c_uint64
libcd.grid_wprojection.argtypes = [array_1d_complex, ctypes.c_int, ctypes.c_double, ctypes.POINTER(VIS_DATA),\
			           ctypes.POINTER(W_KERNEL_DATA)]
def grid_wprojection_func(uvgrid, grid_size, theta, vis_struct, wkern_struct):
    return libcd.grid_wprojection(uvgrid, grid_size, theta, vis_struct, wkern_struct)

# W-projection degridding
# uint64_t degrid_wprojection(struct vis_data *vis, double complex *uvgrid, int grid_size, double theta,struct w_kernel_data *wkern);
libcd.degrid_wprojection.restype  =  ctypes.c_uint64
libcd.degrid_wprojection.argtypes = [ctypes.POINTER(VIS_DATA), array_1d_complex, ctypes.c_int, ctypes.c_double, \
			           ctypes.POINTER(W_KERNEL_DATA)]
def degrid_wprojection_func(vis_struct, uvgrid, grid_size, theta, wkern_struct):
    return libcd.degrid_wprojection(vis_struct, uvgrid, grid_size, theta, wkern_struct)


# AW-projection
#uint64_t grid_awprojection(double complex *uvgrid, int grid_size, double theta, struct vis_data *vis, struct w_kernel_data *wkern,
#                           struct a_kernel_data *akern, int bl_min, int bl_max);
libcd.grid_awprojection.restype  =  ctypes.c_uint64
libcd.grid_awprojection.argtypes = [array_1d_complex, ctypes.c_int, ctypes.c_double, ctypes.POINTER(VIS_DATA),\
			           ctypes.POINTER(W_KERNEL_DATA),ctypes.POINTER(A_KERNEL_DATA), ctypes.c_int, ctypes.c_int]
def grid_awprojection_func(uvgrid, grid_size, theta, vis, wkern, akern, bl_min, bl_max):
    return libcd.grid_awprojection(uvgrid, grid_size, theta, vis, wkern, akern, bl_min, bl_max)

# Filling hermitian part of the uvgrid
# void make_hermitian(double complex *uvgrid, int grid_size)
libcd.make_hermitian.restype = None
libcd.make_hermitian.argtypes = [array_1d_complex, ctypes.c_int]
def make_hermitian_func(uvgrid, grid_size):
    return libcd.make_hermitian(uvgrid, grid_size)

# FFT shift function
# void fft_shift(double complex *uvgrid, int grid_size)
libcd.fft_shift.restype = None
libcd.fft_shift.argtypes = [array_1d_complex, ctypes.c_int]
def fft_shift_func(uvgrid, grid_size):
    return libcd.fft_shift(uvgrid, grid_size)

# Uniform weighting. First argument (uint array) is not used outside the function
# void weight(unsigned int *wgrid, int grid_size, double theta, struct vis_data *vis)
libcd.weight.restype = None
libcd.weight.argtypes = [array_1d_uint, ctypes.c_int, ctypes.c_double,ctypes.POINTER(VIS_DATA)]
def weight_func(grid_size, theta, out_struct):
    wgrid = np.zeros(grid_size*grid_size, dtype=np.uint)
    if not wgrid.flags['C_CONTIGUOUS']:
    	wgrid = np.ascontiguousarray(wgrid)
    return libcd.weight(wgrid, grid_size, theta, out_struct)

# void convolve_aw_kernels(struct bl_data *bl,struct w_kernel_data *wkern,struct a_kernel_data *akern);
libcd.convolve_aw_kernels.restype = None
libcd.convolve_aw_kernels.argtypes = [ctypes.POINTER(BL_DATA), ctypes.POINTER(W_KERNEL_DATA), ctypes.POINTER(A_KERNEL_DATA)]
def convolve_aw_kernels_func(bl_data, wkern, akern):
    return libcd.convolve_aw_kernels(bl_data, wkern, akern)

# Check the grid size w.r.t. the actual uv values
# int check_grid_size(struct vis_data *vis, double lambda);
libcd.check_grid_size.restype  =  ctypes.c_int
libcd.check_grid_size.argtypes = [ctypes.POINTER(VIS_DATA), ctypes.c_double]
def check_grid_size_func(vis, uvlambda):
    status = libcd.check_grid_size(vis, uvlambda)
    if status != 0:
        print('ERROR: actual dataset grid size {} is more than the supplied uvlambda {}'.format(status, int(uvlambda)))
    return status

# int fill_vis_data(struct vis_data *vis, int nant, int ntime, int nfreq)
libcd.fill_vis_data.restype = ctypes.c_int
libcd.fill_vis_data.argtypes = [ctypes.POINTER(VIS_DATA), ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
def fill_vis_data_func(vis_data, nant, nbl, ntime, nfreq):
    return libcd.fill_vis_data(vis_data, nant, nbl, ntime, nfreq)

# int copy_vis_data(struct vis_data *vis_out, struct vis_data *vis_in, int clear_vis)
libcd.copy_vis_data.restype = ctypes.c_int
libcd.copy_vis_data.argtypes = [ctypes.POINTER(VIS_DATA), ctypes.POINTER(VIS_DATA),ctypes.c_int ]
def copy_vis_data_func(vis_out, vis_in, clear_vis=0):
    return libcd.copy_vis_data(vis_out, vis_in, clear_vis)

#int print_bl_vis_data(struct vis_data *vis, int bl)
libcd.print_bl_vis_data.restype = ctypes.c_int
libcd.print_bl_vis_data.argtypes = [ctypes.POINTER(VIS_DATA),ctypes.c_int ]
def print_bl_vis_data_func(vis_in, bl):
    return libcd.print_bl_vis_data(vis_in, bl)

#int fill_stats(struct vis_data *vis);
libcd.fill_stats.restype = ctypes.c_int
libcd.fill_stats.argtypes = [ctypes.POINTER(VIS_DATA) ]
def fill_stats_func(vis):
    return libcd.fill_stats(vis)

#int wkernel_allocate(struct w_kernel_data *wkern, int plane_count, int size_x, int size_y, int oversampling)
libcd.wkernel_allocate.restype = ctypes.c_int
libcd.wkernel_allocate.argtypes = [ctypes.POINTER(W_KERNEL_DATA), ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
def wkernel_allocate_func(w_kernel_data, plane_count, size_x, size_y, oversampling):
    return libcd.wkernel_allocate(w_kernel_data, plane_count, size_x, size_y, oversampling)










